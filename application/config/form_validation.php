<?php 
	$config = array(
		'order/add' => array(
			array('field' => 'nama','label' => 'NAMA','rules' => 'required'),
			array('field' => 'detil','label' => 'DETIL','rules' => 'required')
		),
		'order/delete' => array(
			array( 'field' => 'id', 'label' => 'ID ORDER', 'rules' => 'required' )
		),
		'home/login' => array(
			array( 'field' => 'username', 'label' => 'Username', 'rules' => 'required' ),
			array( 'field' => 'password', 'label' => 'Password', 'rules' => 'required' ),
		)
	);
?>