 <?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct() {
		 parent::__construct();
		 $this->load->helper(array('form', 'url'));
		 $this->load->model('pesanan');
	}

	public function index(){
		// query ke tabel order
		// var_dump(NAMA_DOMAIN);
		$list_order	= $this->pesanan->findAll();
		$data = array(
			"list_order" => $list_order,
			"test" => "Ini value variable test",
			"coba" => "ini valunya coba"
		);
		$data_header = array(
			"list_menu" => array(
				array( 'nama' => "Home", 'link' => site_url('order/index') ),
				array( 'nama' => "List", 'link' => site_url('order/list') ),
				array( 'nama' => "Tambah", 'link' => site_url('order/add') ),
				array( 'nama' => "Hapus", 'link' => site_url('order/delete') ),
				// array( 'nama' => "Log Out", 'link' => site_url('home/logout') )
			),
			"link" => site_url('home/logout'),
			"active" => "Home"
		);

		$this->load->view('template/head');
		
		$this->load->view('template/header', $data_header);
		var_dump($this->session->logged_in);
		if($this->session->logged_in === FALSE || $this->session->logged_in === NULL) { 
			$this->load->view('login_page');
		} else {
			$this->load->view('order', $data);
		}
		$this->load->view('template/footer');
	}

	public function add(){
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
 
		$data_header = array(
			"list_menu" => array(
				array( 'nama' => "Home", 'link' => site_url('order/index') ),
				array( 'nama' => "List", 'link' => site_url('order/list') ),
				array( 'nama' => "Add", 'link' => site_url('order/add') )
			),
			"active" => "Home"
		);
		$this->load->view('template/header', $data_header);
		if ($this->form_validation->run() === FALSE) {
			$this->load->view('add_order');
			$this->load->view('template/footer');
		} else {
			echo 'data sudah diterima';
			$input_data = $this->input->post();
			$nama			= $input_data['nama'];
			$detil		= $input_data['detil'];
			$this->pesanan->store($nama, $detil);
			echo 'Data sudah disimpan';
		}
	}
	
	public function delete(){
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('delete_order');
		} else {
			echo 'data sudah diterima';
			$input_data = $this->input->post();
			echo 'Data deleted';
		}
	}
}
