<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
// JAVA => import bla bla bla

class Home extends CI_Controller {
	public function __construct() {
		 parent::__construct();
		 $this->load->model('pengunjung');
		 $this->load->helper('url');
	}

	public function index() {
		// QUERY GET DATA PENGUNJUNG
		// 1. Get data pengunjung
		// 2. Liat data via var_dump or echo
		// 3. Kirim data tersebut ke VIEW
		$listPengunjung	= $this->pengunjung->findAll();
		var_dump($listPengunjung);
	}

	public function login(){
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		if ($this->session->logged_in === TRUE) {
			return redirect('order', 'refresh');
		}

		var_dump($this->session->logged_in);
		if ($this->form_validation->run() === FALSE) {
			$this->load->view('login_page');
		} else {
			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');
			var_dump($username . ' ' . $password);
			if($username === 'haha' && $password === 'hihi'){
				$sessionData = array( 'username' => $username, 'logged_in' => TRUE );
				$this->session->set_userdata($sessionData);
				return redirect('order', 'refresh');
			} else {
				echo 'Invalid username and password';
			}
		}
	}

	public function logout(){
		if ($this->session->logged_in === TRUE) {
			$this->session->sess_destroy();
		}
		return redirect('home/login', 'refresh');
	}
}
