<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Model {
	  public function __construct() { 
			$this->load->database();
		}

		public function findAll(){
			return $this->db->get_where('order')->result_array();
		}

		public function store($nama, $detil){
			$data['nama'] 	= $nama;
			$data['detil'] 	= $detil;
			$this->db->insert('order', $data);
		}
}
