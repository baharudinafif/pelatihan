<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengunjung extends CI_Model {
	public function __construct(){
		// syntax untuk konek ke DB
		$this->load->database();
	}

	public function findAll(){
		return $this->db->get_where('pengunjung')->result_array();
	}
	
	public function find(){

	}
	
	public function update(){

	}
	
	public function delete(){

	}
	
	public function store(){

	}
}
